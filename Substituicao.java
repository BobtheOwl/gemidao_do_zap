public class Substituicao{
    Jogador entrou;
    Jogador saiu;
    Jogo jogo;
    String tempo;
    /**
     * Cria uma substituicao
     * @param entrou jogador que entrou no jogo
     * @param saiu jogador que saiu do jogo
     * @param jogo em que a substituicao aconteceu
     * @param tempo do jogo em que a substituicao aconteceu
     */
    Substituicao(Jogador entrou, Jogador saiu, Jogo jogo, String tempo){
        this.entrou = entrou;
        this.saiu = saiu;
        this.jogo = jogo;
        this.tempo = tempo;
    }
    /**
     * Cria uma substituicao
     * @param entrou jogador que entrou no jogo
     * @param saiu jogador que saiu do jogo
     * @param jogo em que a substituicao aconteceu
     * @param tempo do jogo em que a substituicao aconteceu
     */
    Substituicao(Jogador entrou, Jogador saiu, Jogo jogo, int tempo){
        this.entrou = entrou;
        this.saiu = saiu;
        this.jogo = jogo;
        if(tempo == 0){
            this.tempo = "Primeiro Tempo";
        }else if(tempo == 1){
            this.tempo = "Segundo Tempo";
        }else if(tempo == 2){
            this.tempo = "Prorrogacao";
        }
    }
    /*Getters e setters */
        /**
         * @return o jogador que entrou em campo
         */
        public Jogador getEntrou() {
            return entrou;
        }
            /**
         * @return o jogador que saiu de campo
         */
        public Jogador getSaiu() {
            return saiu;
        }
        /**
         * @return the jogo
         */
        public Jogo getJogo() {
            return jogo;
        }
        /**
         * @return the tempo
         */
        public String getTempo() {
            return tempo;
        }
}