public class IncompleteChampionshipException extends Exception {
    @Override
    public String getMessage(){
        return "O campeonato nao foi preenchido corretamente!";
    }
}