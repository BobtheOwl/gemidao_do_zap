import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.IOException;

public class Menu {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in); // scanner utilizado para ler as coisas
        FileWriter arquivoRelatorio;
        PrintWriter gravarRelatorio;
        int opcao; // opção do switch case
        
        String nome1, nome2, nome3; // string que será lida
        String text1, text2, text3;// strings auxiliares
        
        int i, j = 0, k, tTimes = 0, tEstadios = 0, tPartidas = 0; // contadores
        int aux1, aux2, aux3;// auxiliares
        
        LinkedList<Time> timesCriados = new LinkedList<Time>();
        Time time, time1, time2;
        
        LinkedList<Jogador> allplayers = new LinkedList<Jogador>();
        Jogador jog;
        
        Tecnico tecnico;
        
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        Jogo partida;
        
        LinkedList<Arbitro> arbitros = new LinkedList<Arbitro>();
        Arbitro arbi;
        LinkedList<Arbitro> arbitrosGenericos = new LinkedList<Arbitro>();
        
        for (i = 0; i < 6; i++) {
            arbi = new Arbitro("arbitro" + (i + 1), "unidade_federativa-" + (i + 1),"funcao" + (i + 1));
            arbitrosGenericos.add(arbi);
        }
        
        LinkedList<Estadio> estadios = new LinkedList<Estadio>();
        Estadio estadio;
        LinkedList<Estadio> estadiosGenericos = new LinkedList<Estadio>();
        
        for (i = 0; i < 4; i++) {
            estadio = new Estadio("estadio" + (i + 1), "unidade_federativa-" + (i + 1));
            estadiosGenericos.add(estadio);
        }
        
        Campeonato copa;
        
        // cadastrando os times da copa no algoritmo
        nome1 = "./times_copa.txt";
        timesCriados = cadastroTimes(nome1);

        while (true) {
            System.out.println("\n\n\n\nOpções Disponíveis(MENU PRINCIPAL):");
            System.out.println("1- Adicionar time;");
            System.out.println("2- Simular partida específica;");
            System.out.println("3- Simular Copa do Mundo(são necessários 4 árbitros);");// FALTA IMPLEMENTAR
            System.out.println("4- Checar relatório da partida(Súmula);");// FALTA IMPLEMENTAR
            System.out.println("5- Criar Estádio;");
            System.out.println("6- Criar árbitro para a Copa do Mundo;");
            System.out.println("7- Finalizar programa.");
            System.out.println("Digite abaixo a opção desejada :");
            opcao = ler.nextInt();
            ler.nextLine();

            switch (opcao) {
            case 1:
                System.out.print("\nDigite o nome do time:");
                nome1 = ler.nextLine();// TEM QUE CRIAR O TIME @!
                time = new Time(nome1);
                for (i = 0; i < 23; i++) {
                    System.out.print("\nDigite a posição do jogador:");
                    nome2 = ler.nextLine();
                    if (nome2.equals("Goleiro") || nome2.equals("goleiro")) {
                        j++;
                        if (j > 3) {
                            System.out.print("\nO time já possui 3 goleiros, jogador não adicionado.\n");
                            i--;
                            continue;
                        }
                    }
                    System.out.print("\nDigite o nome do jogador:");
                    nome3 = ler.nextLine();
                    jog = new Jogador(nome2, nome3);
                    time.addJogador(jog);
                }
                System.out.print("\nDigite o nome do tecnico do time:");
                nome2 = ler.nextLine();// TEM QUE CRIAR O TECNICO @!

                tecnico = new Tecnico(nome2);
                time.addTecnico(tecnico);

                timesCriados.add(time);
                tTimes++;
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 2:
                time1 = null;
                time2 = null;
                estadio = null;
                System.out.print("\nDigite o nome do primeiro time: ");
                nome1 = ler.nextLine();

                time1 = findTime(timesCriados, nome1);

                System.out.print("\nDeseja escolher a escalação(E) ou gerar automaticamente(A)? (E/A):");
                nome1 = ler.nextLine();

                if (nome1.equals("E")) // CASO O CARA QUEIRA ESCOLHER A ESCALACAO
                {
                    for (i = 0; i < 11; i++) {
                        System.out.print("\nEscreva o nome do jogador escalado:"); // VENDO 11 JOGADORES PARA O TIME
                        text1 = ler.nextLine();
                        for (j = 0; j < 23; j++) {
                            if (time1.getJogador(i).getNome().equals(text1))// CHECANDO SE O JOGADOR DIGITADO EXISTE
                            {
                                jog = time1.getJogador(i);
                                time1.addEscalado(jog);
                            }
                        }
                        if (j == 23) {
                            System.out.println("\nJogador não existente, por favor, digite um Jogador existente");
                            i--;
                        }
                    }
                } else {
                    if (nome1.equals("A")) {
                        try{
                            time1.gerarEscalacao();
                        }catch(NeedsMorePlayersException e){
                            System.out.println("Quantidade de times insuficiente!");
                        }
                    } else {
                        System.out.println("\nOpção invalida.");
                    }

                }

                System.out.print("\nDigite o nome do segundo time:");
                nome2 = ler.nextLine();

                time2 = findTime(timesCriados, nome2);

                System.out.print("\nDeseja escolher a escalação(E) ou gerar automaticamente(A)? (E/A):");
                nome2 = ler.nextLine();

                if (nome2.equals("E")) // CASO O CARA QUEIRA ESCOLHER A ESCALACAO
                {
                    for (i = 0; i < 11; i++) {
                        System.out.print("\nEscreva o nome do jogador escalado:"); // VENDO 11 JOGADORES PARA O TIME
                        text2 = ler.nextLine();
                        for (j = 0; j < 23; j++) {
                            if (time2.getJogador(i).getNome().equals(text2))// CHECANDO SE O JOGADOR DIGITADO EXISTE
                            {
                                jog = time2.getJogador(i);
                                time2.addEscalado(jog);
                            }
                        }
                        if (j == 23) {
                            System.out.println("\nJogador não existente, por favor, digite um Jogador existente");
                            i--;
                        }
                    }
                } else {
                    if (nome2.equals("A")) {
                        try{
                            time2.gerarEscalacao();
                        }catch(NeedsMorePlayersException e){
                            System.out.println("Quantidade de times insuficiente!");
                        }
                    } else {
                        System.out.println("\nOpção invalida.");
                    }

                }

                System.out.print("\nDigite o nome do estádio em que a partida será/foi realizada:");
                nome3 = ler.nextLine();

                estadio = findEstadio(estadios, nome3);

                System.out.print("\nDigite o dia em que a partida será/foi realizada:");
                aux1 = ler.nextInt();
                ler.nextLine();

                System.out.print("\nDigite o mes em que a partida será/foi realizada:");
                aux2 = ler.nextInt();
                ler.nextLine();

                System.out.print("\nDigite o horário em que a partida será/foi realizada:");
                text1 = ler.nextLine();

                partida = new Jogo(" ", aux1, aux2, text1, time1, time2, estadio, arbitrosGenericos);
                partida.simularJogo(true);
                tPartidas++;

                jogos.add(partida);

                System.out.print("\nPartida realizada!");

                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;

            case 3:
                copa = new Campeonato("Copa do Mundo 2018");
                for(i=0;i<32;i++){
                    copa.addTime(timesCriados.get(i));
                }
                
                System.out.println("\nDeseja utilizar árbitros genéricos(G) ou árbitros criados(C) para a Copa do Mundo 2018?(G/C)");
                text1 = ler.nextLine();
                if(text1.equals("G")){
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitrosGenericos.get(i));
                    }   
                }else if(text1.equals("C")){
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitros.get(i));
                    }
                }else{
                    System.out.println("\nComo não foi inserido uma opção válida, serão utilizados árbitros genéricos");
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitrosGenericos.get(i));
                    }
                }

                System.out.println("\nDeseja utilizar estádios genéricos(G) ou estádios criados(C) para a Copa do Mundo 2018?(G/C)");
                text1 = ler.nextLine();
                if(text1.equals("G")){
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadiosGenericos.get(i));
                    }   
                }else if(text1.equals("C")){
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadios.get(i));
                    }
                }else{
                    System.out.println("\nComo não foi inserido uma opção válida, serão utilizados estádios genéricos");
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadiosGenericos.get(i));
                    }
                }

                try{
                    copa.gerarGrupos();
                    copa.setEliminatorias(copa.simularFaseGrupos());
                    copa.simularSegundafase();
                    System.out.println("\n"+copa.resultadoCopa());
                }catch(NeedsMoreTeamsException e){
                    System.out.println("Quantidade de times insuficiente!");
                }catch(NeedsMorePlayersException e){
                    System.out.println("Quantidade de jogadores em um dos times insuficiente!");
                }catch(IncompleteChampionshipException e){
                    System.out.println("Campeonato completado incorretamente!");
                }
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 4:// escrevendo em um arquivo txt o relatório da partida
                System.out.print("\nDigite os times que participaram do jogo");
                time1 = null;
                time2 = null;

                System.out.print("\nDigite o nome do primeiro time:");
                text1 = ler.nextLine();
                time1 = findTime(timesCriados, text1);

                System.out.print("\nDigite o nome do segundo time:");
                text2 = ler.nextLine();
                time2 = findTime(timesCriados, text2);

                System.out.print("\nDigite o dia que a partida ocorreu:");
                aux1 = ler.nextInt();
                ler.nextLine();
                try {
                    partida = findJogo(tPartidas, jogos, time1, time2, aux1);
                    arquivoRelatorio = new FileWriter("./" + text1 + "-" + text2 + aux1 + ".txt");
                    gravarRelatorio = new PrintWriter(arquivoRelatorio);
                    if (partida == null) {
                        System.out.println("jogo nao encontrado!");
                        break;
                    }
                    relatorioArq(gravarRelatorio, partida);
                    arquivoRelatorio.close();
                    System.out.println("Súmula da partida criada no diretório corrente!");
                } catch (IOException e) {
                    System.out.println("Arquivo nao encontrado ou incapaz de ser criado!");
                }
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 5:// adicionando estádio
                System.out.print("\nDigite o nome do Estádio:");
                nome1 = ler.nextLine();

                System.out.print("\nDigite o nome da Unidade Federativa do Estádio:");
                nome2 = ler.nextLine();

                estadio = new Estadio(nome1, nome2);
                estadios.add(estadio);

                tEstadios++;
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 6:// adicionando arbitro
                System.out.print("\nDigite o nome do Arbitro da Copa:");
                nome1 = ler.nextLine();
                System.out.print("\nDigite o nome da Unidade Federativa do Arbitro:");
                nome2 = ler.nextLine();
                System.out.print("\nDigite a função do Arbitro:");
                nome3 = ler.nextLine();
                arbi = new Arbitro(nome1, nome2,nome3);
                arbitros.add(arbi);

                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 7:// finalizando programa
                System.out.print("\nFim do programa!\n");
                ler.close();
                return;
            default:
                System.out.print("\nA opção desejada é inválida, digite uma opção existente.");
                break;
            }

        }
    }

    public static Time findTime(LinkedList<Time> times, String nome) {
        int i;
        int j = times.size();
        while (true) {
            for (i = 0; i < j; i++) {
                if (times.get(i).getNome().contentEquals(nome)) {
                    return times.get(i);
                }
            }
            if (i == j) {
                System.out.println(
                        "\nTime não existente, por favor, digite um time existente ou crie um time com o nome desejado.");
                return null;
            }
        }
    }

    public static Estadio findEstadio(LinkedList<Estadio> estadios, String nome) {
        int i;
        int j = estadios.size();
        while (true) {
            for (i = 0; i < j; i++) {
                if (estadios.get(i).getNome().contentEquals(nome)) {
                    return estadios.get(i);
                }
            }
            if (i == j) {
                System.out.print(
                        "\n Estádio não existente, por favor, digite um estádio existente ou crie um estádio com o nome desejado.");
                return null;
            }
        }
    }

    public static Jogo findJogo(int total, LinkedList<Jogo> jogos, Time time1, Time time2, int dia) {
        int i;
        Jogo jogo;
        for (i = 0; i < total; i++) { // condição que checa se o jogo analisado no momento tem como integrantes o
                                      // time1 e o time2
            if ((jogos.get(i).getTime1().equals(time1) && jogos.get(i).getTime2().equals(time2))
                    || (jogos.get(i).getTime2().equals(time1) && jogos.get(i).getTime1().equals(time2))) {
                if (jogos.get(i).getDia() == dia) {
                    jogo = jogos.get(i);
                    return jogo;
                }
            }
        }
        return null;
    }

    public static void relatorioArq(PrintWriter gravar, Jogo partida) {
        gravar.println(partida.relatorio());
    }
//inserindo times do arquivo txt no programa
    public static LinkedList<Time> cadastroTimes(String nome) {
        FileReader arq;
        BufferedReader leitorBuffer;
        String linha, posicao, playerName;
        LinkedList<Time> times = new LinkedList<Time>();
        Jogador jogador;
        Time time;
        int i;

        try {
            arq = new FileReader(nome);
            leitorBuffer = new BufferedReader(arq);

            linha = leitorBuffer.readLine(); // lê a primeira linha, no caso será o nome do time
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto
            while (linha != null) {
                // TENHO A LINHA, AGORA DE ACORDO COM O MEU TXT, INTERPRETAR O TXT
                // PRIMEIRA COISA É O NOME DO TIME, DEPOIS PARA CADA JOGADOR COLOCAR A POSIÇÃO E
                // O NOME DO JOGADOR
                i = 0;
                time = new Time(linha);
                while (i < 23) {
                    linha = leitorBuffer.readLine();
                    posicao = linha;

                    linha = leitorBuffer.readLine();
                    playerName = linha;
                    
                    jogador = new Jogador(playerName,time,posicao);
                    time.addJogador(jogador);
                    i++;
                }
                times.add(time);
                linha = leitorBuffer.readLine(); // lê a próxima linha (no caso será o nome de um time ou nulo)
            }
            arq.close();
            return times;
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        return null;
    }
}

// criar menu para o usuário
