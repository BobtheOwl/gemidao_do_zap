public class NoPlayerException extends Exception {
    @Override
    public String getMessage(){
        return "O jogador nao existe!";
    }
}