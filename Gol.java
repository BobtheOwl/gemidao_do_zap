public class Gol {
    private Jogo jogo;
    private Jogador autor;
    private String tempo;
    /**
     * Cria um Gol
     * @param jogo em que o gol aconteceu
     * @param autor do gol
     * @param tempo do jogo em que o gol aconteceu
     */
    Gol(Jogo jogo, Jogador autor, String tempo){
        this.autor = autor;
        this.jogo = jogo;
        this.tempo = tempo;
    }
    /**
     * Cria um Gol
     * @param jogo em que o gol aconteceu
     * @param autor do gol
     * @param tempo do jogo em que o gol aconteceu
     */
    Gol(Jogo jogo, Jogador autor, int tempo){
        this.autor = autor;
        this.jogo = jogo;
        if(tempo == 0){
            this.tempo = "Primeiro Tempo";
        }else if(tempo == 1){
            this.tempo = "Segundo Tempo";
        }else if(tempo == 2){
            this.tempo = "Prorrogacao";
        }
    }
/*Getters e setters*/
        /**
         * retorna o jogador que fez o gol
         * @return o autor do gol
         */
        public Jogador getAutor() {
            return autor;
        }
        /**
         * @return quando o gol foi feito
         */
        public String getTempo() {
            return tempo;
        }
        /**
         * retorna o jogo em que o gol foi feito
         * @return o jogo em que o gol aconteceu
         */
        public Jogo getJogo() {
            return jogo;
        }
        //para pegar o time que fez o gol, pega o jogador e vê de que time ele e
    }