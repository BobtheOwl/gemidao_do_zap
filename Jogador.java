import java.util.LinkedList;

public class Jogador extends Integrante{
    private LinkedList<Gol> gols;
    private LinkedList<Cartao> cartoes;
    private String posicao;
    /**
     * Cria um jogador
     * @param nome do jogador
     * @param time do jogador
     * @param posicao do jogador
     */
    Jogador(String nome, Time time,  String posicao){
        this.nome = nome;
        this.time = time;
        this.posicao = posicao;
        gols = new LinkedList<Gol>();
        cartoes = new LinkedList<Cartao>();
    }
    /**
     * Cria um jogador
     * @param nome do jogador
     * @param time do jogador
     * @param posicao do jogador
     */
    Jogador(String nome, String posicao){
        this.posicao = posicao;
        this.nome = nome;
        gols = new LinkedList<Gol>();
        cartoes = new LinkedList<Cartao>();
    }
    /**
     * Faz um gol, adicionando-o nos gols que o jogador fez e que o time fez
     * @param jogo em que o gol foi feito
     * @param tempo do jogo em que o gol foi feito
     * @return o gol que o jogador fez
     * @exception NoTeamException se o jogador nao tiver um time
     */
    public Gol fazerGol(Jogo jogo, String tempo) throws NoTeamException{
        try{
            Gol gol = new Gol(jogo, this, tempo);    
            this.gols.add(gol);
            this.time.addGol();
            return gol;
        }catch(NullPointerException e){
            throw new NoTeamException();
        }
    }
    /**
     * Faz um gol, adicionando-o nos gols que o jogador fez e que o time fez
     * @param jogo em que o gol foi feito
     * @param tempo do jogo em que o gol foi feito. 0 para primeiro tempo, 1 para segundo tempo e 2 para prorrogacao
     * @return o gol que o jogador fez
     * @exception NoTeamException se o jogador nao tiver um time
     */
    public Gol fazerGol(Jogo jogo, int tempo) throws NoTeamException{
        try{
            Gol gol = new Gol(jogo, this, tempo);    
            this.gols.add(gol);
            this.time.addGol();
            return gol;
        }catch(NullPointerException e){
            throw new NoTeamException();
        }
    }
    /**
     * Checa quantos cartoes amarelos o jogador recebeu no jogo enviado
     * @param jogo a ser avaliado
     * @return a quantidade de cartoes amarelos no jogo
     */
    public int AmarelosPorJogo(Jogo jogo){
        try{
            int i = 0;
            int qnt = 0;
            for(i = 0; i < this.cartoes.size(); i++){
                if(this.cartoes.get(i).getJogo().equals(jogo)){
                    qnt++;
                }
            }
            return qnt;
        }catch(NullPointerException e){
            System.out.println("Erro! Existe um cartao criado de forma inadequada!");
            return -1;
        }
    }
    /*Getters and Setters*/
        /**
         * @return the cartaoAmarelo
         */
        public LinkedList<Cartao> getCartoes() {
            return cartoes;
        }
        /**
         * @return the gols
         */
        public LinkedList<Gol> getGols() {
            return gols;
        }

}