public class Integrante{
    protected String nome;
    protected Time time;
    /*Getters e setters */
        /**
         * @return o nome do jogador
         */
        public String getNome() {
            return this.nome;
        }
        /**
         * @return o time do jogador
         */
        public Time getTime() {
            return time;
        }
        /**
         * @param nome o novo nome do jogador
         */
        public void setNome(String nome) {
            this.nome = nome;
        }
        /**
         * @param time o novo time do jogador
         */
        public void setTime(Time time) {
            this.time = time;
        }
}