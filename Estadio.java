public class Estadio {
    private String UnidadeFederal;
    private String nome;
    /**
     * Cria um estadio.
     * @param nome do estadio
     * @param UnidadeFederal em que o estadio esta
     */
    Estadio(String nome, String UnidadeFederal){
        this.UnidadeFederal = UnidadeFederal;
        this.nome = nome;
    }
    /*Getters and setters*/
        /**
         * @return o nome do estadio
         */
        public String getNome() {
            return nome;
        }
        /**
         * @return a Unidade Federal em que o estadio esta
         */
        public String getUnidadeFederal() {
            return UnidadeFederal;
        }
        /**
         * @param nome seta o nome
         */
        public void setNome(String nome) {
            this.nome = nome;
        }
        /**
         * @param unidadeFederal seta a unidade federal do estado
         */
        public void setUnidadeFederal(String unidadeFederal) {
            this.UnidadeFederal = unidadeFederal;
        }
        
}