import java.util.LinkedList;
import java.util.Scanner;
import java.io.IOException;
public class Main{
    public static void main(String[] args) {
        Campeonato copa = new Campeonato("Copa do Mundo 2018");
        Arbitro arbitro = new Arbitro("Alberto", "Rio de Janeiro", "Arbitro 1");
        copa.adicionarArbitro(arbitro);
        arbitro = new Arbitro("Bruce", "Sao Paulo", "Arbitro 2");
        copa.adicionarArbitro(arbitro);
        arbitro = new Arbitro("Carlos", "Espirito Santo", "Arbitro 3");
        copa.adicionarArbitro(arbitro);
        arbitro = new Arbitro("Daniel", "Minas Gerais", "Arbitro 4");
        copa.adicionarArbitro(arbitro);
        arbitro = new Arbitro("Eduardo", "Bahia", "Arbitro 5");
        copa.adicionarArbitro(arbitro);
        arbitro = new Arbitro("Fagner", "Sao Paulo", "Arbitro 6");
        copa.adicionarArbitro(arbitro);
        Estadio estadio = new Estadio("Estadio 1", "Sao Paulo");
        copa.adicionarEstadio(estadio);
        estadio = new Estadio("Estadio 2", "Espirito Santo");
        copa.adicionarEstadio(estadio);
        estadio = new Estadio("Estadio 3", "Minas Gerais");
        copa.adicionarEstadio(estadio);
        estadio = new Estadio("Estadio 4", "Bahia");
        copa.adicionarEstadio(estadio);
        int i, j;
        Tecnico tecnico;
        Time time;
        Jogador jogador;
        for(i = 1; i < 33; i++){   
            tecnico = new Tecnico("Tecnico " + i);
            time = new Time("Time " + i);
            time.addTecnico(tecnico);
            for(j = 1; j < 24; j++){
                jogador = new Jogador("Jogador " + j + " Time " + i, "Posicao " + j);
                time.addJogador(jogador);
            }
            copa.addTime(time);
        }
        LinkedList<Jogo> jogos;
        try{
            copa.gerarGrupos();
            copa.setEliminatorias(copa.simularFaseGrupos());
            copa.simularSegundafase();
        }catch(NeedsMoreTeamsException e){
            System.out.println("Quantidade de times insuficiente!");
        }catch(NeedsMorePlayersException e){
            System.out.println("Quantidade de jogadores em um dos times insuficiente!");
        }catch(IncompleteChampionshipException e){
            System.out.println("Campeonato completado incorretamente!");
        }

        System.out.println();
        System.out.println(copa.resultadoCopa());
        System.out.println();
    }
}
