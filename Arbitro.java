import java.util.LinkedList;
//ver se não é melhor usar extend em vez de uma string função
public class Arbitro {
    private String nome;
    private String UF;
    private String funcao;
/**
 * Cria um arbitro
 * @param nome do arbitro
 * @param UF do arbitro
 * @param funcao do arbitro
 */
    Arbitro(String nome, String UF, String funcao){
        this.nome = nome;
        this.UF = UF;
        this.funcao = funcao;
    }
/**
 * Cria um arbitro
 * @param nome do arbitro
 * @param UF do arbitro
 * @param funcao do arbitro
 */
    Arbitro(String nome, String UF){
        this.nome = nome;
        this.UF = UF;
    }
    /**
     * Cria um cartao amarelo e adiciona ele a lista de cartoes do jogador que recebeu o cartao e a lista de cartoes dados no jogo.
     * @param jogador que recebeu o cartao
     * @param jogo em que o cartao foi dado
     * @param tempo do jogo em que o cartao foi dado
     * @return o cartao criado
     * @throws NoGameException se o jogo enviado for nulo
     * @throws NoPlayerException se o jogador enviado for nulo
     */
    public Cartao darCartaoAmarelo(Jogador jogador, Jogo jogo, int tempo) throws NoGameException, NoPlayerException{
        try{
            Cartao cartao = new Cartao("Amarelo", jogo, jogador, tempo);
            jogador.getCartoes().addLast(cartao);
            jogo.getCartoes().add(cartao);
            return cartao;
        }catch(NullPointerException e){
            if(jogador == null)
            throw new NoPlayerException();
            else
            throw new NoGameException();
        }
    }
    /**
     * Cria um cartao vermelho e adiciona ele a lista de cartoes do jogador que recebeu o cartao e a lista de cartoes dados no jogo.
     * @param jogador que recebeu o cartao
     * @param jogo em que o cartao foi dado
     * @param tempo do jogo em que o cartao foi dado
     * @return o cartao criado
     * @throws NoGameException se o jogo enviado for nulo
     * @throws NoPlayerException se o jogador enviado for nulo
     */
    public Cartao darCartaoVermelho(Jogador jogador, Jogo jogo, int tempo) throws NoGameException, NoPlayerException{
        try{
            Cartao cartao = new Cartao("Vermelho", jogo, jogador, tempo);
            jogador.getCartoes().addLast(cartao);
            jogo.getCartoes().add(cartao);
            return cartao;
        }catch(NullPointerException e){
            if(jogador == null)
            throw new NoPlayerException();
            else
            throw new NoGameException();
        }
    }

    //Getters e setters
        /**
         * retorna a funcao do arbitro
         * @return a funcao
         */
        public String getFuncao() {
            return funcao;
        }
        /**
         * retorna o nome do arbitro
         * @return o nome
         */
        public String getNome() {
            return nome;
        }
        /**
         * retorna a unidade federal do arbitro
         * @return a unidade federal
         */
        public String getUF() {
            return UF;
        }
        /**
         * seta a funcao do arbitro
         * @param funcao a nova funcao do arbitro
         */
        public void setFuncao(String funcao) {
            this.funcao = funcao;
        }
        /**
         * seta o nome do aribtro
         * @param nome o novo nome do arbitro
         */
        public void setNome(String nome) {
            this.nome = nome;
        }
        /**
         * seta a unidade federal 
         * @param uF a nova unidade federal do arbitro
         */
        public void setUF(String UF) {
            this.UF = UF;
        }
}
