public class MissingInfoException extends Exception {
    @Override
    public String getMessage(){
        return "Alguma informacao foi enviada errada!";
    }
}